//
//  Album.swift
//  jsonDecodeExample
//
//  Created by Andrew Benson on 9/9/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import Foundation




struct AlbumStats: Codable {
    let views: Int
    let plays: Int
    let downloads: Int
}

struct Covers: Codable {
    let large: URL
    let medium: URL
    let thumb: URL
}

struct Album: Codable {
    let id: Int
    let title: String
    let description: String?
    let released: Bool
    let released_at: Date
    let short_url: URL
    let various_artists: Bool
    let instrumental: Bool
    let accessible: Bool
    let downloadable: Bool
    let itunes_url: URL?
    let youtube_url: URL?
    let slug: String
    let videos: [Video]
    let stats: AlbumStats
    let covers: Covers
    let tracks: [Track]
    let artists: [Artist]
    let hosts: [Host]?
}

struct Track: Codable {
    let id: Int
    let album_id: Int
    let title: String
    let artist: String
    let duration: Int
    let position: Int
    let audio_url: URL
    let url: URL
    let videos: [String]
    let licensed: Bool
    let released_at: Date
    let streams: Int
    let downloads: Int
    let slug: String

}

struct Artist: Codable {
    let id: Int
    let displayname: String
    let twitter: String?
}

struct Host: Codable {
    let id: Int
    let displayname: String
    let twitter: String?

}


struct AlbumReference: Codable {
    let type: String
    let id: Int
}

struct Video: Codable {
    let id: Int
    let title: String
    let description: String?
    let view_count: Int
    let like_count: Int
    let dislike_count: Int
    let stream_url: URL
    let duration: TimeInterval
    let aspect_ratio: String
    let published_at: Date
    let attached_to: AlbumReference
    let thumbnail: [Int:URL]

}


struct MuxAsset: Codable {
    let asset_id: String
    let playback_id: String
}
