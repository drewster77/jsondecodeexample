//
//  ViewController.swift
//  jsonDecodeExample
//
//  Created by Andrew Benson on 9/9/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {

    var albums: [Album]!

    override func viewDidLoad() {
        super.viewDidLoad()

        // EVERYWHERE YOU SEE AN EXCLAMATION POINT IN THIS FILE IS BAD FORM.
        // DO NOT DO THAT -- HANDLE ERRORS AND SETUP PROPERLY

        // find our file
        let url = Bundle.main.url(forResource: "whatever", withExtension: "json")!

        // load data from our file
        let data = try! String(contentsOf: url)
        let jsonData = data.data(using: .utf8)!

        // Decode into our megastruct
        let decoder = JSONDecoder()

        // Fix it so dates decode correctly
        decoder.dateDecodingStrategy = .iso8601

        // Try and decode it -- this is all or nothing.  Either it will 100% work or 100% fail.
        albums = try! decoder.decode([Album].self, from: jsonData)

        print("Got here... spin up the debugger")

        for album in albums {
            print("Album '\(album.title)':")
            let cover = album.covers.thumb
            print("    Cover: \(cover)")
            for track in album.tracks {
                let title = track.title
                let artist = track.artist

                print("        '\(title)' by \(artist)")
            }
        }
        print("ok")


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

